from rest_framework import serializers
import json
from .models import RegistroTrafico, Intruso, IntrusoIp, Ataque, TipoAtaque

class IntrusoSerializer(serializers.ModelSerializer): 
    class Meta:
        model = Intruso
        fields = ['intruso_id','nombre','ip']
        
class RegistroTraficoSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegistroTrafico
        fields = '__all__'

class TipoAtaqueSerielizer(serializers.ModelSerializer): 
    class Meta:
        model = TipoAtaque
        fields = '__all__'


class AtaqueSimpleSerializer(serializers.ModelSerializer): 
    tipo = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='nombre'
    )
    intruso = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='nombre'
    )
    class Meta:
        model = Ataque
        fields = ['intruso','tipo']

class IntrusoRegistroTraficoSerializer(serializers.Serializer):
    intruso = IntrusoSerializer(read_only=True)
    trafico = RegistroTraficoSerializer(read_only=True)


class IntrusoSerializer(serializers.ModelSerializer): 
    class Meta:
        model = Intruso
        fields = ['intruso_id','nombre','ip']


