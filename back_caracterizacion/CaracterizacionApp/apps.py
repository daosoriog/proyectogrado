from django.apps import AppConfig


class CaracterizacionappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CaracterizacionApp'
