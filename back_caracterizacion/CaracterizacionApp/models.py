from django.db import models

# Create your models here.

class RegistroTrafico(models.Model):
    registro_id = models.AutoField(primary_key=True)
    fecha = models.DateTimeField()
    url_path = models.TextField()
    body = models.TextField()
    ip_origen = models.CharField(max_length=40)
    ip_destino = models.CharField(max_length=40)
    protocolo = models.CharField(max_length=40)
    puerto = models.IntegerField()
    def __str__(self):
        return str(self.registro_id)+ '-' + str(self.fecha) +'-'+self.url_path

    def get_str_compare(self):
        return self.fecha.strftime("%m/%d/%Y, %H:%M:%S") +'-'+self.url_path+'-'+self.body

class Intruso(models.Model):
    intruso_id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=40)
    ip = models.CharField(max_length=40)
    def __str__(self):
        return str(self.nombre)

class IntrusoIp(models.Model):
    intruso_ip = models.ForeignKey(Intruso, on_delete=models.CASCADE)
    ip = models.CharField(max_length=40)

class TipoAtaque(models.Model):
    tipo_ataque_id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    def __str__(self):
        return str(self.nombre)

class Ataque(models.Model):
    registro_trafico = models.OneToOneField(
        RegistroTrafico,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    tipo = models.ForeignKey(TipoAtaque, on_delete=models.CASCADE)
    intruso = models.ForeignKey(Intruso, on_delete=models.CASCADE)

