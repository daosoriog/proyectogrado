from django.shortcuts import render
from django.core import serializers
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework import generics, viewsets, status
from rest_framework.response import Response
from .serializers import IntrusoSerializer, RegistroTraficoSerializer, IntrusoRegistroTraficoSerializer, AtaqueSimpleSerializer
from .report import Report
from datetime import datetime, timedelta
import base64

from .models import RegistroTrafico, Intruso, IntrusoIp, Ataque, TipoAtaque


# Create your views here.


class IntrusosLista(generics.ListCreateAPIView):
    queryset = Intruso.objects.all()
    serializer_class = IntrusoSerializer

class RegistroTraficoLista(generics.ListCreateAPIView):
    queryset = RegistroTrafico.objects.all()
    serializer_class = RegistroTraficoSerializer

class IntrusoTrafico(APIView):
    def get(self, request, *args, **kwargs):
        intrusos  = Intruso.objects.all()
        data_r = []

        for intr in intrusos:
            rt = RegistroTrafico.objects.filter(ip_origen__contains=intr.ip).order_by('-fecha')[:100]
            print(len(rt))

            data_intr = {}
            data_intr['intruso'] = IntrusoSerializer(intr).data
            data_intr['trafico'] = []
            for r in rt:
                att = Ataque.objects.filter(registro_trafico__registro_id=r.registro_id)
                if(len(att) > 0):
                    at1 = att.get()
                    data_rts = { **RegistroTraficoSerializer(r).data, **{'tipo':at1.tipo.nombre, 'intruso': at1.intruso.nombre}}
                else:
                    data_rts = { **RegistroTraficoSerializer(r).data, **{'tipo':'Normal', 'intruso': intr.nombre}}

                data_intr['trafico'].append(data_rts)
            data_r.append(data_intr)

        return Response(data_r, status=status.HTTP_200_OK)


class MapaServicios(APIView):
    def get(self, request, *args, **kwargs):
        servicios = [{'nombre':'Web','puerto':'80', 'estado': 'inactivo', 'ataques':[]},
                     {'nombre':'SSH','puerto':'22', 'estado': 'inactivo', 'ataques':[]},
                     {'nombre':'PostgreSQL','puerto':'5432', 'estado': 'inactivo', 'ataques':[]},
                     {'nombre':'MongoDB','puerto':'8', 'estado': 'inactivo', 'ataques':[]}
                     ]
        for serv in servicios:
            print(serv)
            rt = RegistroTrafico.objects.filter(ip_destino__contains=':'+serv['puerto'],
                                                fecha__range=[(datetime.now() -  timedelta(days = 1)), datetime.now()]).order_by('-fecha')
            print(len(rt))
            if len(rt) > 0:
                serv['estado'] = 'activo' 
            for r in rt:
                atts = Ataque.objects.filter(registro_trafico__registro_id=r.registro_id)
                if len(atts) > 0:
                    serv['estado'] = 'ataque' 
                for att in atts:
                    serv['ataques'].append({'tipo':att.tipo.nombre, 'intruso': att.intruso.nombre})

        print(servicios)
        return Response(servicios, status=status.HTTP_200_OK)

class GenReporte(APIView):
    def post(self, request, *args, **kwargs):
        print(request.data)
        fi = datetime.strptime(request.data['fecha-inicio'], "%Y-%m-%dT%H:%M:%S")
        ff = datetime.strptime(request.data['fecha-fin'], "%Y-%m-%dT%H:%M:%S")
        trafico = RegistroTrafico.objects.filter(fecha__range=[fi,ff]).order_by('fecha')
        data_report = {}
        item_tmp = {}
        rt_tmp = None
        print(fi)
        print(ff)
        print(trafico)
        
        '''
        intrus:
            servicio:
                normal:
                    path:
                        path1
                        path2
                    fechas:
                        fecha1
                        fecha2
            servicio:
                normal:
                    path:
                        path1
                        path2
                    fechas:
                        fecha1
                        fecha2
                normal:
                    path:
                        path1
                        path2
                    fechas:
                        fecha1
                        fecha2
                ataque
                    path:
                        path1
                    fechas:
                        fecha1
        '''
        intruso_tmp = ''
        for rt in trafico:
            print('--------------rt----------')
            print(rt.ip_origen)
            print(rt)
            print(rt.protocolo)

            intruso = Intruso.objects.filter(ip=rt.ip_origen.split(':')[0]).first()
            print(intruso)
            intruso_tmp =  intruso.nombre
            if intruso.nombre not in data_report.keys():
                data_report[intruso.nombre] = []
            atts = Ataque.objects.filter(registro_trafico__registro_id=rt.registro_id)
            if rt_tmp is None:
                #normal?
                if len(atts) > 0:
                    att = atts.first()
                    item_tmp[rt.protocolo] = {att.tipo.nombre:{'paths':[], 'fechas':[]}}
                    item_tmp[rt.protocolo][att.tipo.nombre]['paths'].append(rt.url_path +' - Body: '+ rt.body)
                    item_tmp[rt.protocolo][att.tipo.nombre]['fechas'].append(rt.fecha)
                    data_report[intruso.nombre].append({rt.protocolo: [item_tmp[rt.protocolo]]})
                    print('attttttttttttttttttttttttttttttttt1111')
                    print(att.tipo.nombre)
                    item_tmp = {}
                else:
                    item_tmp[rt.protocolo] = {'normal':{'paths':[], 'fechas':[]}}
                    #item_tmp[rt.protocolo]['normal']['fechas'] = []
                    item_tmp[rt.protocolo]['normal']['paths'].append(rt.url_path)
                    item_tmp[rt.protocolo]['normal']['fechas'].append(rt.fecha)


            else:
                print(rt.fecha - rt_tmp.fecha )
                if len(atts) > 0:
                    att = atts.first()
                    if len(item_tmp) > 0:
                        item_tmp[rt_tmp.protocolo]['normal']['fechas'].append(rt_tmp.fecha)
                        data_report[intruso.nombre].append({rt_tmp.protocolo: [ item_tmp[rt_tmp.protocolo] ]})
                        print('dddddddddddddddddddddd')
                        print({rt_tmp.protocolo: [ item_tmp[rt_tmp.protocolo] ]})
                        item_tmp = {}
                    print('attttttttttttttttttttttttttttttttt')
                    print(att.tipo.nombre)
                    item_tmp[rt.protocolo] = {att.tipo.nombre:{'paths':[], 'fechas':[]}}
                    item_tmp[rt.protocolo][att.tipo.nombre]['paths'].append(rt.url_path +' - Body: '+ rt.body)
                    item_tmp[rt.protocolo][att.tipo.nombre]['fechas'].append(rt.fecha)
                    data_report[intruso.nombre].append({rt.protocolo: [item_tmp[rt.protocolo]]})
                    print({rt.protocolo: [item_tmp[rt.protocolo][att.tipo.nombre]]})
                    item_tmp = {}
                else:
                    if len(item_tmp) == 0:
                        item_tmp[rt.protocolo] = {'normal':{'paths':[], 'fechas':[]}}
                    if rt_tmp.protocolo == rt.protocolo: # and tiempos , 2 min 
                        item_tmp[rt.protocolo]['normal']['paths'].append(rt.url_path)
                        fecha_tmp = rt.fecha
                    else:
                        print('cambiaaaaaaaaa')
                        item_tmp[rt_tmp.protocolo]['normal']['fechas'].append(rt_tmp.fecha)
                        data_report[intruso.nombre].append({rt_tmp.protocolo: [ item_tmp[rt_tmp.protocolo] ]})
                        print({rt_tmp.protocolo: [ item_tmp[rt_tmp.protocolo] ]})
                        item_tmp = {}
                        item_tmp[rt.protocolo] = {'normal':{'paths':[], 'fechas':[]}}
                        item_tmp[rt.protocolo]['normal']['paths'].append(rt.url_path)
                        item_tmp[rt.protocolo]['normal']['fechas'].append(rt.fecha)
                        #data_report[intruso.nombre].append({rt.protocolo: [item_tmp[rt.protocolo][att.tipo.nombre]]})
                        #item_tmp = {}
            rt_tmp = rt
        print(rt_tmp)
        print(item_tmp)
        if len(item_tmp) > 0:
            item_tmp[rt_tmp.protocolo]['normal']['fechas'].append(fecha_tmp)
            data_report[intruso_tmp].append({rt_tmp.protocolo: [item_tmp[rt_tmp.protocolo]]})

        #print(data_report)
        report_obj = Report()
        html_rp  = report_obj.createHtmlFromJson(data_report,fi,ff)
        pdf_file = report_obj.makepdf(html_rp,"tmp/reporte.pdf")
        print(type(pdf_file))
        pdf_file = base64.b64encode(pdf_file)

        #print(cr_file)



            #data_report[intruso.nombre].append() 


        return Response({'status':'Ok', 'file_name':'reporte.pdf','fileb64': pdf_file}, status=status.HTTP_200_OK)
