from django.contrib import admin
from .models import Intruso, IntrusoIp

# Register your models here.

@admin.register(Intruso)
class IntrusoAdmin(admin.ModelAdmin):
  list_display = ['intruso_id', 'nombre','ip']

