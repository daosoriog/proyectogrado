import sys
import pandas as pd
import xmltodict as xd
from log_watcher.watcher_handler import MyHandler
from log_watcher.watcher import Watcher
from data_handler.sorter import TrafficSorter

if __name__=="__main__":

    df = None

    #   Se carga el dataset de trafico normal
    for i in [1,2,3,4]:
        print(i)
        with open('../../../randomForest/attacksDataSets/allNormals'+str(i)+'.xml') as f:
            d = xd.parse(f.read())
        if df is None:
            df = pd.DataFrame(d['dataset']['sample'])
        else:
            df = pd.concat([df,pd.DataFrame(d['dataset']['sample'])])

    #   Se cargan los datasets de ataques
    for i in [1,2,3,4,5,6,7]:
        print(i)
        with open('../../../randomForest/attacksDataSets/allAttacks'+str(i)+'.xml') as f:
            d = xd.parse(f.read())
        if df is None:
            df = pd.DataFrame(d['dataset']['sample'])
        else:
            df = pd.concat([df,pd.DataFrame(d['dataset']['sample'])])

    ts = TrafficSorter()
    ts.set_data_frame(df)
    df = ts.process_df_from_xml()
    random_forest_clf = ts.train_random_forest()

    my_handler = MyHandler()
    my_handler.set_traffic_sorter(ts)
    w = Watcher("./logs/", my_handler)
    #my_handler.set_grid_search()
    w.run()
