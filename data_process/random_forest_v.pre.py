from lxml import etree
import xmltodict as xd
import pandas as pd
from sklearn.preprocessing import OneHotEncoder

#import xml.etree.ElementTree as ET
def prettyprint(element, **kwargs):
    xml = etree.tostring(element, pretty_print=True, **kwargs)
    print(xml.decode(), end='')



#data = ET.parse('../allAttacks1.xml')
with open('allAttacks1.xml') as f:
    d = xd.parse(f.read())
  #data = etree.parse(f)

print(d['dataset']['sample'][0]);

df = pd.DataFrame(d['dataset']['sample'])
print(df.head())
print(df.isnull().sum())

#root = data.getroot()

#print(data)
#prettyprint(data)
#root = etree.Element("root")
#print(root.tag)
#print(root[0])
#prettyprint(root[0])

#child = root[0]
#print(child.tag)

#print(df['type'])

with pd.option_context('display.max_rows', 20, 'display.max_columns', None):  # more options can be specified also
    print(df)


#todos los request tienen el mismo portocolo?


print(df.loc[0]) # Row
#print([v['protocol'] != 'HTTP/1.1' for v in df['request']])


#Extrae  columnas de request para tarabajar
df['method'] = [v['method'] for v in df['request']]
df['protocol'] = [v['protocol'] for v in df['request']]
df['path'] = [v['path'] for v in df['request']]
df['headers'] = [v['headers'] for v in df['request']]
df['body'] = [v['body'] for v in df['request']]


one_hot = OneHotEncoder()
encoded = one_hot.fit_transform(df[['protocol']])
df['protocol_n'] = encoded.toarray()

one_hot = OneHotEncoder()
encoded = one_hot.fit_transform(df[['method']])
df['method_n'] = encoded.toarray()

#Extrae  ataque y tipo  de la columna label#df['type'] = df['label']['type'].map({'attack': 1,'normal': 0})
df['type'] = [v['type'] for v in df['label']]
df['type'] = df['type'].map({'attack': 1,'normal': 0})
df['type'] = [v['type'] for v in df['label']]
df['type_attack'] = [v['attack'] for v in df['label']]

#Elimina columnas sin importancia
#df.drop(columns=['label','request','@id'],axis=1, inplace=True)
df2=df.iloc[:, 2:]

print(df.isnull().sum())
print(df.head())
print(df2.tail())
#print(df['type'])

########
########
# otra forma de clasificar que no me sirvio, revisar por que puede funcionar mejor


#grid_search = GridSearchCV(estimator=clf, param_grid=param_grid, cv=3, n_jobs=-1)
#grid_search.fit(X_train, y_train)

#best_params = grid_search.best_params_
#print("Best parameters:", best_params)

# Train the best model
#co_clf = grid_search.best_estimator_

#y_pred = co_clf.predict(X_test)

#accuracy = accuracy_score(y_test, y_pred)
#print(f"Accuracy: {accuracy * 100:.2f}%\n")

#print(classification_report(y_test, y_pred))

