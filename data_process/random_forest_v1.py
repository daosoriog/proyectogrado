from lxml import etree
import xmltodict as xd
import pandas as pd
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split
#from sklearn.feature_extraction.text import CountVectorizer # Verctorizar texto
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score, classification_report
import sys

#data = ET.parse('../allAttacks1.xml')
df = None
#   Se cargan los datasets de ataques
for i in [1,2,3,4,5]:
    print(i)
    with open('../../../randomForest/attacksDataSets/allAttacks'+str(i)+'.xml') as f:
        d = xd.parse(f.read())
    if df is None:
        df = pd.DataFrame(d['dataset']['sample'])
    else:
        df = pd.concat([df,pd.DataFrame(d['dataset']['sample'])])

#print(pd.concat(df))
#print(df.head())




#   Se carga el dataset de trafico normal
with open('../../../randomForest/attacksDataSets/allNormals1.xml') as f:
    d = xd.parse(f.read())
df = pd.concat([df,pd.DataFrame(d['dataset']['sample'])])


print(df)
print(df.isnull().sum())


#todos los request tienen el mismo portocolo?


print(df.loc[0]) # Row
print('%%%%%%%%%%%%%%%%%%%%%%%%%')
print('%%%%%%%%%%%%%%%%%%%%%%%%%')
print(df.iloc[: 1:1]) 
#print([v['protocol'] != 'HTTP/1.1' for v in df['request']])


#Extrae  columnas de request para tarabajar
df['path'] = [v['path'] for v in df['request']]
#df['body'] = [v['body'] for v in df['request']]
df['body'] = [v['body'] if 'body' in v else '' for v in df['request']]

#Extrae  ataque y tipo  de la columna label
# Y puede tener dos columnas?
df['type'] = [v['type'] for v in df['label']]
df['type_attack'] = [v['attack'] if 'attack' in v else 'normal' for v in df['label']]

#Elimina columnas sin importancia
df=df.iloc[:, 3:]
print(df.isnull().sum())

print(df.tail())
#Asignacion de comulnas: existe cada caracarter?
featuresIn = ['&', '%', '/', '\\', '+', "'", '?', '!', ';', '#', '=', '[', ']', '$',
'(', ')', '^', '*', ',', '-', '<', '>', '@', '_', ':', '{', '}', ' ',
'.', '|', '"', '<>', '==', '&#', 'document', 'window',
'iframe', 'location', 'this', 'onload', 'onerror',
'createelement', 'string.fromcharcode', 'search', 'div',
'img', '<script', 'src', 'href', 'cookie', 'var', 'eval()',
'http', '.js',
'–', '/**/', '%', '(', ')', '∧',
'*', 'char', ',', '-', '<', '>', ' ', '.', '|', '”', '<>', '<=', '>=',
'&&', '||', ':', '!=', 'count', 'into', 'or', 'and', 'not',
'null', 'select', 'union', 'insert', 'update', 'delete',
'drop', 'replace', 'all', 'any', 'from', 'count', 'user',
'where', 'sp', 'xp', 'like', 'exec',
'../', '..\\', 'etc', 'passwd', '\\.', '\\/', './', '/', ':', '//', ':/',
'system', 'ini', '..', 'exec', ':\\', '%00', '.bat', 'file',
'windows', 'boot', 'winnt', '.conf', 'access', 'log', ',,',
'\\', '*', '(', ')', '/', '+', '<', '>', ';', '”', '&', '|', '(&', '(|',
')(', ',', '!', '=', ')&', ' ', '*)', '))', '&(', '+)', '=)', 'cn=',
'sn=', '=*', '(|', 'mail', 'objectclass', 'name',
'/∗', '%', '+', "'", ';', '#', '=', '[', ']', '(', ')', '^', '∗', '()',
'//', ',', '-', '<', '>', '.', '|', '”', '<>', '<=', '>=', '&&',
'||', '::', '((', '<--', ' ', 'or', 'count', 'path/', 'and', 'not',
'text()', 'child', 'position()', 'node()', 'name', 'user',
'comment',
'<!-', '-->', '#', '+', ',', 'etc/', '/passwd', 'dir',
'#exec', 'cmd', 'fromhost', 'email', 'odbc', '#include',
'virtual', 'bin/', 'toaddress', 'message', 'replyto',
'sender', '#echo', 'httpd', 'access.log', 'var', '+connect',
'date_gmt', '+statement', 'log/', '/mail', 'mail', 'id',
'+id', '.bat', 'ls+', 'home/', 'winnt\\', 'system.ini',
'.conf', '+-l', 'windows', '.conf', '.com', ':\\',
'../', '..\\', 'etc', 'passwd', '\\.', '\\/', './', ':', ':/', '.',
'system32', 'display', '.exe', 'cmd', 'dir', ';', 'tmp/',
'etc/passwd', 'wget', 'cat', 'ping', 'bash', 'ftp', '|', '..',
'exec', ':\\', '.bat', 'file', 'script', 'rm ', 'c:', 'winnt',
'access', 'log',  'www.', 'http', ' ', 'bin/', 'telnet',
'echo', 'root', '-aux', 'shell', 'uname', 'IP',
'%0A', '%0D', '%0D%0A', 'SET', 'COOKIE', ':', '+',
'TAMPER'                         
]
## Se limpian los duplicados

features = []
for element in featuresIn:
    if element not in features:
        features.append(element)

print(df)
sys.exit(0)


count=0
for f in features:
    count=count+1
    df['exist_'+f+'_path'] = df['path'].str.contains(str(f), regex=False)
    df['exist_'+f+'_path'] = df['exist_'+f+'_path'].map({True: 1, False: 0})
    df['exist_'+f+'_body'] = df['path'].str.contains(str(f), regex=False)
    df['exist_'+f+'_body'] = df['exist_'+f+'_body'].map({True: 1, False: 0})

#Elimina columnas path y body, no se puede texto solo numerico
df=df.iloc[:, 2:]
print(df.isnull().sum())
print(df.head())


print(df.iloc[:, 2:])   ##### X -> Datos de entrada
print(df.iloc[:, 1:2])    ##### Y -> Datos de salida

X_train, X_test, y_train, y_test = train_test_split(df.iloc[:, 2:], df.iloc[:, 1:2], test_size=0.15, random_state=42)

clf = RandomForestClassifier(random_state=42)

'''


clf.fit(X_train,y_train)


predictions = clf.predict(X_test)
print("Accuracy:",metrics.accuracy_score(y_test, predictions))
'''

param_grid = {
    'n_estimators': [250, 500],
    'max_leaf_nodes': [8, 10, 12],
}
grid_search = GridSearchCV(estimator=clf, param_grid=param_grid, cv=3, n_jobs=-1)
grid_search.fit(X_train, y_train.values.ravel())

best_params = grid_search.best_params_
print("Best parameters:", best_params)

# Train the best model
co_clf = grid_search.best_estimator_

y_pred = co_clf.predict(X_test)

accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy: {accuracy * 100:.2f}%\n")

print(classification_report(y_test, y_pred))

