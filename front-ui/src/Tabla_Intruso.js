import 'bootstrap/dist/css/bootstrap.min.css';
//import './interfaz.css'; // Asegúrate de tener el archivo interfaz.css en la ruta adecuada
import React, { useState, useEffect } from 'react';
import axios from 'axios';

const DynamicTableCarousel = () => {

  const [data, setData] = useState([]);
  const fetchData = async () => {
    try {
      const response = await axios.get('intruso-trafico/'
      );
	    console.log(response.data);
      setData(response.data); // Suponiendo que los datos se reciben en un array de objetos
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    // Llamada inicial a la API
    fetchData();
    // Configurar temporizador para actualizar cada 3 minutos
    const interval = setInterval(() => {
      fetchData();
    }, 3000); //  en milisegundos
    // Limpiar temporizador al desmontar el componente
    return () => clearInterval(interval);
  }, []); // Solo se ejecuta al montar el componente


	const [currentIndex, setCurrentIndex] = useState(0);

	const nextSlide = () => {
		setCurrentIndex((prevIndex) => (prevIndex === data.length - 1 ? 0 : prevIndex + 1));
	};

	const prevSlide = () => {
		setCurrentIndex((prevIndex) => (prevIndex === 0 ? data.length - 1 : prevIndex - 1));
	};
	console.log(data.length)
  var count = 0;
  const slides = [];
  for(let intruso of data){
    if(count != 0)
      slides.push(<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to={count} aria-label="Slide {count}"></button>);
    count = count + 1;
  }

	return (
		<div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
      <div className="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
        {slides}
      </div>
      <div className="carousel-inner" bg-dark>
      {data.map((item, index) => (
        <div key={index} className={index === currentIndex ? 'carousel-item active' : 'carousel-item'}>
          <h3>{item.intruso.nombre}</h3>
          <table className="table table-dark">
            <thead>
              <tr>
                <th>Estado</th>
                <th>Servicio</th>
                <th>Tipo</th>
                <th>Fecha/Hora</th>
                <th>Objetivo</th>
                <th>URL Path</th>

              </tr>
            </thead>
            <tbody>
            {item.trafico.map((servicio, i) => (
              <tr key={i}>
                <td><div className={servicio.tipo != 'Normal' ? "alert alert-danger" : "alert alert-success as-tabla-intruso"} role="alert">{servicio.tipo != 'Normal' ? 'Ataque!' : "Normal"}</div></td>
                <td>{servicio.protocolo}</td>
                <td>{servicio.tipo}</td>
                <td>{servicio.fecha}</td>
                <td>{servicio.ip_destino}</td>
                <td>{servicio.url_path}</td>
              </tr>
            ))}
            </tbody>
          </table>
        </div>
		))}
      </div>
      <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev" onClick={prevSlide}>
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="sr-only">Previous</span>
      </a>
      <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next" onClick={nextSlide}>
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="sr-only">Next</span>
      </a>
		</div>
	);
};

export default DynamicTableCarousel;
        
