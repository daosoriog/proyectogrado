import axios from 'axios';
import logo from './logo.svg';
import './App.css';
import DynamicTableCarousel from './Tabla_Intruso.js';
import BarraNavegacion from './BarraNavegacion.js';
import TableMap from './TableMap.js';
import ReportModal from './ReportModal.js';
//import setupProxy from './ProxyDjango.js';

{/* importar librerias */}


function App() {
	//setupProxy();
	console.log('pasa');
	//app.use(cors());
	/*
	app.use(cors({
		origin: "http://localhost:3000",
		allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept"],
	}));
	*/
  return (
    <div>
      <BarraNavegacion />
	  <div className="container my-5 ">
		<div className="row">
			<div className="col-sm-12  col-md-3 col-lg-3  col-xl-3">
				<TableMap />
				<br />
	  			<ReportModal />
			</div>

			<div id="intruso-tabla" className="col-sm-12  col-md-9 col-lg-9  col-xl-9">
			     <DynamicTableCarousel />
			</div>
		</div>
	  </div>
    </div>
  );
}

export default App;
