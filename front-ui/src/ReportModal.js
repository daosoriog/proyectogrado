import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import dayjs from "dayjs";


const ReportModal = () => {
  const [show, setShow] = useState(false);
  const [fechaIni, setFechaIni] = useState('');
  const [fechaFin, setFechaFin] = useState('');

  const handleClose = () => setShow(false);
  const handleShow = () => {
    const actualdate = dayjs(new Date()).format("YYYY-MM-DDTHH:mm:ss");
    setFechaIni(actualdate);
    setFechaFin(actualdate);
    setShow(true);
  };
  const genReport = () => {
    console.log(fechaIni);

    axios.post('reporte/', {
        'fecha-inicio' : fechaIni,
        'fecha-fin' : fechaFin
      }
    ).then((response) => {
	    const downloadLink = document.createElement("a");
      console.log(response.data.file_name);
	    downloadLink.href = "data:application/pdf;base64," + response.data.fileb64;
	    downloadLink.download = response.data.file_name;
	    downloadLink.click();
    });
  };

  return (
    <div className="container" id='report-container'>
      <h3>Reporte</h3> 

      <Button variant="primary" onClick={handleShow}>
        Generar
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Generar Reporte</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className="form-group">
              <label className="col-form-label">Fecha de inicio:</label>
              <input type="datetime-local" className="form-control" id="fecha-inicio" 
                onChange={event => setFechaIni(event.target.value)}
                value={fechaIni}
              />
            </div>
            <div className="form-group">
              <label className="col-form-label">Fecha fin:</label>
              <input  type="datetime-local" className="form-control" id="fecha-fin" 
                onChange={event => setFechaFin(event.target.value)}
                value={fechaFin}
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cerrar
          </Button>
          <Button variant="primary" onClick={genReport}>
            Generar
          </Button>
        </Modal.Footer>
      </Modal>
  
    </div>



  );
};

export default ReportModal;
