import React, { useState, useEffect } from 'react';

const BarraNavegacion = () => {

	return (
	    <div>
	       {/* Barra de navegación */}
	      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
		<div className="container-fluid">
		  <a className="navbar-brand">User:Admin</a>
		  <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
		    <span className="navbar-toggler-icon"></span>
		  </button>
		  <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
			  <button  type="button" >
			    <span >R</span>
			  </button>
		    <div className="navbar-nav ms-auto">
		      <a className="nav-link active" aria-current="index.html" href="index.html">Home</a>
		      <a className="nav-link" href="page_Intruder.html">Intruder</a>
		      <a className="nav-link" href="page_servis.html">Servis</a>
		      <a className="nav-link active" aria-current="page" href="Page_Report.html">Report</a>
		    </div>
		  </div>
		</div>
	      </nav>  
	    </div>
	);
};

export default BarraNavegacion;
