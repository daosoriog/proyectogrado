import React, { useState, useEffect } from 'react';
import axios from 'axios';

const TableMap = () => {

	const [data, setData] = useState([]);

  const fetchData = async () => {
    try {
      const response = await axios.get('mapa-servicios/');
	    console.log(response.data);
      setData(response.data); // Suponiendo que los datos se reciben en un array de objetos
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    // Llamada inicial a la API
    fetchData();
    // Configurar temporizador para actualizar cada 3 minutos
    const interval = setInterval(() => {
      fetchData();
    }, 3000); //  en milisegundos
    // Limpiar temporizador al desmontar el componente
    return () => clearInterval(interval);
  }, []); // Solo se ejecuta al montar el componente


        const cellStatus = [{'nombre':'Web','imagen':'URL', 'estado':'ataque'},
        {'nombre':'SSH','imagen':'URL', 'estado':'activo'},
        {'nombre':'PostgresSQL','imagen':'URL', 'estado':'activo'},
		{'nombre':'mongodb','imagen':'URL', 'estado':'inactivo'}
                             ];
	const estado_tipo = {'activo': "alert alert-info", "ataque":"alert alert-danger", "inactivo":"alert alert-secondary"}
    const toggleCellStatus = (index) => {


        //CellStatus[index] = !CellStatus[index];
        //setCellStatus(CellStatus);

      };

  return (
    <div className="container" id='mapa-container'>
      <h2>Mapa de servicios</h2> 
      {data.map((item, index) => (
        <div className={estado_tipo[item.estado]}  role="alert">
          {item.nombre}
        </div>
        ))}
    </div>
  );
};

export default TableMap;
