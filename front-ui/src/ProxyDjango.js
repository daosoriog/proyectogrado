import { createProxyMiddleware } from 'http-proxy-middleware';

const setupProxy = (app) => {
	app.use(
		createProxyMiddleware('/api', {
			target: 'http://localhost:8000',
			changeOrigin: true,
			pathRewrite: { '^/api': '' },
		})
	);
}
export default setupProxy;
